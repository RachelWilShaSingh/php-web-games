$( document ).ready( function() {

    var timeToRefresh = 10;
    var pageRefresh = setInterval( RefreshTimer, 1000 );
    var countIdle = 0;
    var pageIdle = null;

    if ( $( "#never-refresh" ).val() == "true" )
    {
        StopRefresh();
        StopIdle();
        $( "#refresh-info" ).addClass( "invisible" );
        $( "#idle-info" ).addClass( "invisible" );
        $( "#update-info" ).removeClass( "invisible" );
    }
    else
    {
        $( "#refresh-counter" ).html( timeToRefresh );
    }

    function RefreshPage() {
        $( "#invisible" ).submit();
    }

    function RefreshTimer() {
        timeToRefresh -= 1;
        console.log( timeToRefresh );

        $( "#refresh-counter" ).html( timeToRefresh );

        if ( timeToRefresh == 0 ) {
            RefreshPage();
        }
    }

    function StopRefresh() {
        clearInterval( pageRefresh );
        timeToRefresh = 0;
        $( "#refresh-counter" ).html( timeToRefresh );
        $( "#refresh-info" ).addClass( "invisible" );
        $( "#idle-info" ).removeClass( "invisible" );
        $( "#idle-counter" ).html( countIdle );

        if ( pageIdle == null ) {
            pageIdle = setInterval( IdleTimer, 1000 );
        }
    }

    function ResetIdleCounter() {
        countIdle = 0;
        if ( pageIdle == null ) {
            pageIdle = setInterval( IdleTimer, 1000 );
        }
    }

    function IdleTimer() {
        countIdle += 1;
        $( "#idle-counter" ).html( countIdle );

        if ( countIdle == 20 ) {
            // Restart refresh timer
            timeToRefresh = 10;
            pageRefresh = setInterval( RefreshTimer, 1000 );
            $( "#refresh-counter" ).html( timeToRefresh );
            clearInterval( pageIdle );
            $( "#refresh-info" ).removeClass( "invisible" );
            $( "#idle-info" ).addClass( "invisible" );
            pageIdle = null;
        }
    }

    function StopIdle() {
        clearInterval( pageIdle );
    }

    $( "input[type=text]" ).change( function() {
        StopRefresh();
        ResetIdleCounter();
    } );

    $( "#stop-refresh" ).click( function() {
        StopRefresh();
        StopIdle();
        $( "#refresh-info" ).addClass( "invisible" );
        $( "#idle-info" ).addClass( "invisible" );
        $( "#update-info" ).removeClass( "invisible" );
    } );

    $( this ).keypress( function() {
        StopRefresh();
        ResetIdleCounter();
    } );

    var currentlyDragging = "none";

    $( "#view-debug" ).click( function() {
        if ( $( "#debug-holder" ).css( "display" ) == "none" ) {
            $( "#debug-holder" ).slideDown( "fast", function() { } );
        } else {
            $( "#debug-holder" ).slideUp( "fast", function() { } );
        }
    } );

    $( ".draggable" ).mousedown( function() {
        console.log( "Mouse down" );
        console.log( $( this ) );
        currentlyDragging = $( this ).attr( "id" );

        $( this ).addClass( "highlight" );
        StartMovement( $( this ), currentlyDragging );
    } );

    $( ".tile" ).mousedown( function() {
        if ( currentlyDragging != "none" ) {
            var left = $( this ).css( "left" );
            var top = $( this ).css( "top" );

            $( "#" + currentlyDragging ).css( "left", left );
            $( "#" + currentlyDragging ).css( "top", top );

            // Update hidden fields for post
            $( "#move-id" ).val( currentlyDragging );
            $( "#move-left" ).val( left );
            $( "#move-top" ).val( top );

            console.log( "Store", currentlyDragging, left, top );

            //$( ".tile" ).removeClass( "highlight" );
            $( "#" + currentlyDragging ).removeClass( "highlight" );


            currentlyDragging = "";
            $( "#tip-window" ).html( "" );

            SavePositions();
        }
    } );

    function getRandom( min, max ) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function rollDie( dtype ) {
        if ( dtype.toString().includes( "d" ) )
        {
            dtype = parseInt( dtype.replace( "d", "" ) );
        }
        return getRandom( 1, dtype );
    }

    $( ".roll-die" ).click( function() {
        var dieType = $( this ).attr( "id" );

        var maxValue = parseInt( dieType.replace( "roll-d", "" ) );
        var random = rollDie( maxValue );

        $( "#roll-result" ).val( random );
        $( "#last-roll" ).val( "d" + maxValue );
    } );


    function SavePositions() {
        $( "#game-board" ).submit();
    }

    function StartMovement( obj, id ) {
        $( "#tip-window" ).html( "Click on a tile to move <strong>" + currentlyDragging + "<strong>." );
    }
} );
