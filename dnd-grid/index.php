<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> DND Grid </title>
  <meta name="description" content="Place pieces and move them around">
  <meta name="author" content="Rachel Singh">

  <link rel="stylesheet" href="assets/style.css">
  <link rel="icon" type="image/png" href="assets/favicon.png">

  <script src="assets/jquery-3.5.0.min.js"></script>
  <script src="assets/script.js"></script>
</head>

<body>
    
    <? include_once( "logic.php" ); ?>
    
    <? if ( isset( $_GET["norefresh"] ) ) { ?>
        <input type="hidden" id="never-refresh" value="true">
    <? } else { ?>
        <input type="hidden" id="never-refresh" value="false">
    <? } ?>

    <form method="post" id="game-board">
        <div class="grid-view" style="width: <?=$viewWidth?>;">
            <?
            for ( $y = 0; $y < $mapHeight; $y++ ) {
                for ( $x = 0; $x < $mapWidth; $x++ ) {
                    $xpos = ($x * $tilewh) . "px";
                    $ypos = ($y * $tilewh) . "px";
                    ?> 
                    <div class="tile" id="x_<?=$x?>_y_<?=$y?>" title="x: <?=$x?> y: <?=$y?>" style="width: <?=$tilewhpx?>; height: <?=$tilewhpx?>; left: <?=$xpos?>; top: <?=$ypos?>;"></div>
                    <?
                }
            }
            ?>
            
            <? foreach ( $game->sessionArray["characters"] as $key => $character ) { 
                $xpos = ($character["x"] * $tilewh) . "px";
                $ypos = ($character["y"] * $tilewh) . "px";
                $deadclass = "";
                
                // Soft delete
                if ( $character["deleted"] == true ) { continue; }
                
                // CSS change for dead characters
                if ( isset( $character["dead"] ) && $character["dead"] == true ) {
                    $deadclass = "dead";
                }
                ?>
            <div class="character-options draggable <?=$deadclass?>" id="<?=$character["name"]?>" style="left: <?=$xpos?>; top: <?=$ypos?>;">
                <div class="name"><?=$character["name"] ?></div>
                <div class="sprite"><img src="assets/<?=$character["image"]?>"></div>
                <? if ( $character["dead"] ) { ?>
                    <input type="submit" class="kill-object" name="revive-character[<?=$character['name']?>]" value="😇" title="Revive <?=$character["name"]?>">
                <? } else { ?>
                    <input type="submit" class="kill-object" name="kill-character[<?=$character['name']?>]" value="💀" title="Kill <?=$character["name"]?>">
                <? } ?>
                <? if ( $character["pc"] == false ) { ?>
                    <input type="submit" class="remove-object" name="delete-character[<?=$character['name']?>]" value="❎" title="Delete <?=$character["name"]?>">
                <? } ?>
            </div>
            <? } ?>
        </div>
        
        <input type="hidden" id="move-id" name="move-id" value="">
        <input type="hidden" id="move-left" name="move-left" value="">
        <input type="hidden" id="move-top" name="move-top" value="">
        <input type="hidden" id="move-character" name="move-character" value="move-character">
    </form>
    
    <div id="tip-window">
        asdf
    </div>
    
    <div class="control-panel">
        <h2>Reset board</h2>
        <form method="post">
            <p>Reset so that only the Player Characters are on the board</p>
            <input type="submit" name="reset-board" value="Reset board">
        </form>
        <hr>        
        
        <h2>Add enemies</h2>
        <form method="post" class="add-enemy">
            <p>
                Add
                <input type="text" name="addEnemy[amount]" value="" class="number" placeholder="5">
                enemies. named <input type="text" name="addEnemy[name]" value="" class="name" placeholder="Enemy">
                <br>
                <input type="checkbox" name="addEnemy[random]" value="true" id="addEnemyRandom">
                <label for="addEnemyRandom">Place randomly</label>
                <br><br>
                <input type="submit" name="add-enemies" value="Add 'em!">
            </p>
        </form>
        
        <hr>
        
        <h2>Move characters</h2>
        <p>
            Click on a character on the grid, then click on another space to move them to.
            (Clicking and dragging doesn't work.)
        </p>
        
        <hr>
        
        <h2>Kill/Revive/Delete</h2>        
        <p>
            ❎ = Delete NPC <br>
            💀 = Kill character <br>
            😇 = Revive character
        </p>
        
        <hr>
        
        <h2>Auto-refresh</h2>
        <p id="refresh-info" class="">This page will auto-refresh in <span id="refresh-counter">x</span> seconds.
        <sub>The game board will only update after a refresh, or after the game state is modified by you.</sub>
        </p>
        <p id="idle-info" class="invisible">You have been idle for <span id="idle-counter">x</span> seconds.
        <br><sub>Refresh counter will re-set after 20 seconds.</sub>
        </p>
        <p id="update-info" class="invisible">You will need to manually refresh the page to see updates made by other players.</p>
        <input type="button" id="stop-refresh" value="Stop auto-refresh">
        <p><a href="http://www.moosader.com/web-games/dnd-grid?norefresh=true">NEVER AUTO-REFRESH</a></p>
        
        
        <form id="invisible" method="post"></form>
        
        <hr>
        
        <h2>Debug</h2>
        <input type="button" id="view-debug" value="Toggle debug log">
        <div id="debug-holder">
<pre class="debug">
POST:
<? print_r( $_POST ); ?>

SESSION:
<? print_r( $game->sessionArray ); ?>

STATUS:
<? print_r( $game->status ); ?>
</pre>
        </div>
    </div>
    
    <div class="die-pane">
        <table>
            <tr>
                <th>Roll Helper</th>
                <th>Roll Die</th>
            </tr>
            <tr>                
                <td><!-- result -->
                    Result: <input type="text" id="roll-result">
                    Roll type: <input type="text" id="last-roll">
                </td>
                
                <td><!-- dice -->
                    <input type="button" id="roll-d4" class="roll-die" value="d4">
                    <input type="button" id="roll-d6" class="roll-die" value="d6">
                    <input type="button" id="roll-d8" class="roll-die" value="d8">
                    <input type="button" id="roll-d10" class="roll-die" value="d10">
                    <input type="button" id="roll-d12" class="roll-die" value="d12">
                    <input type="button" id="roll-d20" class="roll-die" value="d20">
                    <input type="button" id="roll-d100" class="roll-die" value="d100">
                </td>
            </tr>
        </table>
    </div>

</body>
</html>
