<?


$tilewh = 100;
$mapWidth = 15;
$mapHeight = 8;

$viewWidth = $mapWidth * $tilewh . "px";
$tilewhpx = $tilewh . "px";


function Output( $label, $text ) {
    echo( "<!-- \n" );
    echo( $label . ": " . $text );
    echo( "\n-->" );
}

function OutputArray( $label, $arr ) {
    echo( "<!-- \n" );
    echo( $label . "\n" );
print_r( $arr );
    echo( "\n-->" );
}


class Game
{
    public $sessionPath;
    public $session;
    public $sessionArray;
    public $status;
    public $tilewh;
    public $mapWidth;
    public $mapHeight;
    
    function __construct() {        
        $this->sessionPath = "sessions";
        $this->session = "dnd";
        $this->sessionArray = null;
        $this->status = array();
        $this->tilewh = 100;
        $this->mapWidth = 15;
        $this->mapHeight = 8;
        
        // Load latest with changes
        $this->LoadGameFile();
        
        $this->Log( "Check POSTs" );
        
        // Check stuff
        if ( isset( $_POST["add-enemies"] ) ) {
            $this->AddEnemies( $_POST["addEnemy"] );
        }
        
        if ( isset( $_POST["move-character"] ) ) {
            $index = $this->GetIndexOfCharacterWithName( $_POST["move-id"] );            
            $this->MoveCharacter( $index, $_POST["move-left"], $_POST["move-top"] );
        }
        
        if ( isset( $_POST["delete-character"] ) ) {
            $id = $this->WhichCharacter( $_POST["delete-character"] );
            $this->Log( "Delete character named \"" . $id . "\"" );
            $index = $this->GetIndexOfCharacterWithName( $id );
            $this->DeleteCharacter( $index );
        }
        
        if ( isset( $_POST["kill-character"] ) ) {
            $id = $this->WhichCharacter( $_POST["kill-character"] );
            $this->Log( "Kill character named \"" . $id . "\"" );
            $index = $this->GetIndexOfCharacterWithName( $id );
            $this->KillCharacter( $index );
        }
        
        if ( isset( $_POST["revive-character"] ) ) {
            $id = $this->WhichCharacter( $_POST["revive-character"] );
            $this->Log( "Revive character named \"" . $id . "\"" );
            $index = $this->GetIndexOfCharacterWithName( $id );
            $this->ReviveCharacter( $index );
        }
        
        if ( isset( $_POST["reset-board"] ) ) {
            $this->InitGame();
        }
        
        // Load latest with changes
        $this->LoadGameFile();
    }
    
    function Log( $data ) {
        array_push( $this->status, $data );
    }
    
    function GetIndexOfCharacterWithName( $name ) {
        for ( $index = 0; $index < sizeof( $this->sessionArray["characters"] ); $index++ ) {
            if ( $this->sessionArray["characters"][$index]["name"] == $name ) {
                return $index;
            }
        }
        return -1;
    }
    
    function WhichCharacter( $arr ) {
        // Technically there should only be one thing in here.
        foreach( $arr as $key => $value ) {
            return $key;
        }
    }
    
    function KillCharacter( $index ) { 
        $this->Log( "Kill character #" . $index );
        
        $this->sessionArray["characters"][$index]["dead"] = true;
        
        $this->SaveGameFile();
    }
    
    
    function ReviveCharacter( $index ) { 
        $this->Log( "Revive character #" . $index );
        
        $this->sessionArray["characters"][$index]["dead"] = false;
        
        $this->SaveGameFile();
    }
    
    function DeleteCharacter( $index ) {
        $this->Log( "Delete character #" . $index );
        $deleteKeys = array();
        
        $this->sessionArray["characters"][$index]["deleted"] = true;
        
        $this->SaveGameFile();
    }
    
    function MoveCharacter( $index, $left, $top ) {
        $this->Log( "Move Character" );
        
        $x = str_replace( "px", "", $left );
        $y = str_replace( "px", "", $top );
        $x /= $this->tilewh;
        $y /= $this->tilewh;
        
        $this->Log( "X: " . $x );
        $this->Log( "Y: " . $y );
        $this->Log( "tilewh: " . $this->tilewh );
        $this->Log( "id: " . $index );
        $this->Log( "Total characters: " . sizeof( $this->sessionArray["characters"] ) );
        
        if ( $index != -1 ) {
            $this->Log( "Update character #" . $index );
            $this->sessionArray["characters"][$index]["x"] = $x;
            $this->sessionArray["characters"][$index]["y"] = $y;
        }
        
        $this->SaveGameFile();
    }
    
    function AddEnemies( $infoArray ) {
        $totalEnemies = $this->GetEnemyCount();
        $this->Log( "Add " . $infoArray["amount"] . " enemies named " . $infoArray["name"] . "; total currently is " . $totalEnemies );
        
        if ( $infoArray["name"] == "" ) {
            $infoArray["name"] = "Enemy";
        }
        
        $randomx = 0;
        $randomy = 1;
        for ( $i = 0; $i < $infoArray["amount"]; $i++ ) {
            if ( $infoArray["random"] == true ) {
                $randomx = rand( 0, $this->mapWidth-1 );
                $randomy = rand( 0, $this->mapHeight-1 );
            }
            
            $this->Log( "Add enemy " . $i . " to position (" . $randomx . ", " . $randomy . ")." );
            
            $enemy = array(
                "image" => "sprite-enemy.png",
                "name" => $infoArray["name"] . " " . ( $totalEnemies + $i ),
                "pc" => false,
                "dead" => false,
                "deleted" => false,
                "x" => $randomx,
                "y" => $randomy
            );
            
            array_push( $this->sessionArray["characters"], $enemy );
            
            if ( !isset( $infoArray["random"] ) || $infoArray["random"] == false ) {
                $randomx++;
            }
        }
        $this->SaveGameFile();
    }
    
    function GetEnemyCount() {
        $enemies = 0;
        foreach ( $this->sessionArray["characters"] as $key => $character ) {
            if ( $character["pc"] == false ) {
                $enemies += 1;
            }
        }
        return $enemies;
    }
    
    
    function SaveGameFile() {
        $this->Log( "Save the game file" );
        $jsonString = json_encode( $this->sessionArray );
        file_put_contents( $this->GetSessionPath(), $jsonString );
    }
    
    function LoadGameFile() {
        $this->Log( "Load the game file" );
        if ( !file_exists( $this->GetSessionPath() ) ) {
            $this->InitGame();
        }
        $jsonString = file_get_contents( $this->GetSessionPath() );
        $this->sessionArray = json_decode( $jsonString, true );
    }
    
    function InitGame() {
        $this->Log( "Initialize game" );
        $this->sessionArray = array(
            "characters" => array( 
                "0" => array(
                    "image" => "sprite-bigsby.png",
                    "name" => "Bigsby",
                    "pc" => true,
                    "dead" => false,
                "deleted" => false,
                    "x" => 0,
                    "y" => 0
                ),
                "1" => array(
                    "image" => "sprite-bog.png",
                    "name" => "Bog",
                    "pc" => true,
                    "dead" => false,
                "deleted" => false,
                    "x" => 1,
                    "y" => 0
                ),
                "2" => array(
                    "image" => "sprite-fain.png",
                    "name" => "Fane",
                    "pc" => true,
                    "dead" => false,
                "deleted" => false,
                    "x" => 2,
                    "y" => 0
                ),
                "3" => array(
                    "image" => "sprite-riff.png",
                    "name" => "Riff",
                    "pc" => true,
                    "dead" => false,
                "deleted" => false,
                    "x" => 3,
                    "y" => 0
                ),
                "4" => array(
                    "image" => "sprite-terpomo.png",
                    "name" => "Terpomo",
                    "pc" => true,
                    "dead" => false,
                "deleted" => false,
                    "x" => 4,
                    "y" => 0
                )
            )
        );
        $this->SaveGameFile();
    }
    
    function GetSessionPath() {
        return $this->sessionPath . "/" . $this->session . ".json";
    }
}

$game = new Game();

?>
