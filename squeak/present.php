
<!-- head -->

<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  
  <title> Squeak PRESENTER - Moosadee.com </title>
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">

  <link href="https://fonts.googleapis.com/css2?family=Baloo+Paaji+2:wght@400;800&display=swap" rel="stylesheet"> 
  
  <link rel="stylesheet" href="../../web-assets/style/2020-04.css">
  <link rel="stylesheet" href="../../web-assets/style/2020-04-mobile.css">
  
  <link rel="icon" type="image/png" href="../../web-assets/images/favicon-moose.png">

  <style type="text/css">
      p.debug { display: none; }
      table { width: 100%; 
        border-collapse: collapse;
        font-size: 20pt;
      }
      table, tr, th, td {
        border: solid 1px #000;
      }
  </style>

</head>

<? 
include_once( "logic.php" ); 
$game = new SqueakGame();
$question = $game->GetQuestion();
$answers = $game->GetAllAnswers();
?>

<body>
    <section class="page-body">

        <section class="navigation-holder cf" style="width:90%;">
            <h2>Squeak</h2>
                        
            <div class="playgame" style="text-align: center;">     
              
                <form method="post">
                  <p>Current question: <input type="text" style="width: 50px;" name="question" value="<?= ($game->questionIndex ) ?>"> <sub>(0 thru <?=sizeof( $game->questions ) ?>)</sub></p>
                  <p><input type="submit" value="VIEW QUESTION" name="question-index" style="padding: 10px; font-size:16pt;"></p>
                  
                  <? if ( isset( $_POST["show"] ) ) { ?>
                      <table>
                        <tr><th>Answer</th><th>Submitted by...</th></tr>
                        <? foreach( $answers as $item ) { ?>
                          <tr>
                            <td><?
                            $fitb = str_replace( "___", "<strong>" . $item["answer"] . "</strong>", $question );
                            echo( $fitb );
                            ?></td>
                            <td><?=$item["name"]?></td>
                          </tr>
                        <? } ?>
                      </table>
                  <? } ?>
                  
                  <p style="font-size: 40pt;"><?= $question ?></p>
                  <p><input type="submit" value="SHOW ANSWERS!" name="show" style="padding: 10px; font-size:16pt;"></p>
                </form>
              
            </div>

        </section>
	
</body>
