
<!-- head -->

<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  
  <title> Squeak ADMIN - Moosadee.com </title>
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">

  <link href="https://fonts.googleapis.com/css2?family=Baloo+Paaji+2:wght@400;800&display=swap" rel="stylesheet"> 
  
  <link rel="stylesheet" href="../../web-assets/style/2020-04.css">
  <link rel="stylesheet" href="../../web-assets/style/2020-04-mobile.css">
  
  <link rel="icon" type="image/png" href="../../web-assets/images/favicon-moose.png">

  <style type="text/css">
      /*p.debug { display: none; }*/
      table { width: 100%; 
        border-collapse: collapse;
      }
      table, tr, th, td {
        border: solid 1px #000;
      }
      
  </style>

</head>

<? 
include_once( "logic.php" ); 
$game = new SqueakGame();
$answers = $game->GetAllAnswers();
?>

<body>
    <section class="page-body">

        <section class="navigation-holder cf">
            <h2>Squeak Admin</h2>
            
            <? if ( isset( $_POST["submission"] ) ) { ?>
            <div class="send-confirm">
                <p>Last answer submitted: <?=$_POST["answer"]?></p>
            </div>
            <? } ?>
            
            <div class="playgame" style="text-align: center;">
                <form method="post">
                    <table>
                      <tr><th>Answer</th><th>Submitted by...</th></tr>
                      <? foreach( $answers as $item ) { ?>
                        <tr>
                          <td><?=$item["answer"]?></td>
                          <td><?=$item["name"]?></td>
                        </tr>
                      <? } ?>
                    </table>
                    
                    <input type="submit" name="clear-all" value="Clear">
                </form>
            </div>
            
            <pre>
              <? print_r( $answers); ?>
            </pre>
            
        </section>
	
</body>
