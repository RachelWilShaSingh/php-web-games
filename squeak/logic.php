<?
// Yes it's a Quiplash clone but meant for middle/high school kids
// and meant to be quick to jump in and out of in ten minutes
// before we begin with class

class SqueakGame
{
  public $dataPath;
  public $questions;
  public $questionIndex;
  
  function __construct() {
    $this->dataPath = "data";
    
    $this->GenerateQuestions();
    $this->questionIndex = 0;
    
    if ( isset( $_POST["question"] ) ) {
      $this->questionIndex = $_POST["question"];
    }
    
    if ( isset( $_POST["submission"] ) ) {
      // Look at name and answer, each name gets its own folder in the data path
      $this->SaveAnswer( $_POST["name"], $_POST["answer"] );
    }
    else if ( isset( $_POST["clear-all"] ) ) {
      $this->ClearAll();
    }
  }
  
  function GenerateQuestions() {
    $this->questions = array( 
      "A class you wouldn't want to get an A+ in is ___",
      "The worst flavor of cake imagineable is ___",
      "In the most boring video game ever you would ___",
      "You would be pretty offended to receive ___ as a birthday gift",
      "In the hot new anime of the year, two highschoolers battle ___",
      "The secret ingredient in mom's fry sauce is ___",
      "In the latest, coolest KPop band, all of the band members are actually ___",
      "You have overdue library fees for that copy of ___ you have checked out",
      "If you could make a class on anything it would be about ___"
    );
  }
  
  function SaveAnswer( $name, $answer ) {
    $this->DebugEcho( "SaveAnswer" );
    
    $directoryPath = $this->dataPath . "/" . $name;
    
    $this->DebugEcho( "Directory path: " . $directoryPath );
    
    if ( !is_dir( $directoryPath ) ) {
      mkdir( $directoryPath );
    }
    
    $filePath = $directoryPath . "/answer.txt";
    
    $this->DebugEcho( "File path: " . $filePath );
    $this->DebugEcho( "Text: " . $answer );
    
    $fileHandle = fopen( $filePath, "w" );
    fwrite( $fileHandle, $answer );
    fclose( $fileHandle );
  }
  
  function GetAllAnswers() {
    $this->DebugEcho( "GetAllAnswers" );
    
    $answers = array();
    $folders = scandir( $this->dataPath );
    foreach( $folders as $name ) {
      $folderPath = $this->dataPath . "/" . $name;
      if ( $name == "." || $name == ".." || !is_dir( $folderPath ) ) { 
          continue; 
      }
      $answerPath = $folderPath . "/answer.txt";
      $answer = file_get_contents( $answerPath );
      $data = array( "answer" => $answer, "name" => $name );
      array_push( $answers, $data );
    }
    return $answers;
  }
  
  function ClearAll() {
    $this->DebugEcho( "ClearAll" );
    
    $answers = array();
    $folders = scandir( $this->dataPath );
    foreach( $folders as $name ) {
      $this->DebugEcho( "Folder: " . $name );
      
      $folderPath = $this->dataPath . "/" . $name;
      if ( $name == "." || $name == ".." || !is_dir( $folderPath ) ) { 
          continue; 
      }
      $answerPath = $folderPath . "/answer.txt";
    
      unlink( $answerPath );
      rmdir( $folderPath );
    }
  }
    
  function GetQuestion() {
    return $this->questions[ $this->questionIndex ];
  }
  
  function DebugEcho( $text ) {
    echo( "<p class='debug'>" . $text . "</p>" );
  }
}

?>
