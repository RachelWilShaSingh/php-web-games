
<!-- head -->

<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  
  <title> Squeak PLAYER - Moosadee.com </title>
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">

  <link href="https://fonts.googleapis.com/css2?family=Baloo+Paaji+2:wght@400;800&display=swap" rel="stylesheet"> 
  
  <link rel="stylesheet" href="../../web-assets/style/2020-04.css">
  <link rel="stylesheet" href="../../web-assets/style/2020-04-mobile.css">
  
  <link rel="icon" type="image/png" href="../../web-assets/images/favicon-moose.png">

  <style type="text/css">
      p.debug { display: none; }
      table { width: 100%; }
  </style>

</head>

<? 
include_once( "logic.php" ); 
$game = new SqueakGame();
$question = $game->GetQuestion();
?>

<body>
    <section class="page-body">

        <section class="navigation-holder cf">
            <h2>Squeak</h2>
                        
            <? if ( isset( $_POST["submission"] ) ) { ?>
            <div class="send-confirm">
                <p>Last answer submitted: <?=$_POST["answer"]?></p>
            </div>
            <? } ?>
            
            <div class="playgame" style="text-align: center;">
                <form method="post">
                    <p>Your Username:</p>
                    <p><input type="text" value="<?=$_POST['name']?>" name="name"></p>
                    <p>Your Answer:</p>
                    <p><input type="text" value="" name="answer"></p>
                    <p><input type="submit" value="SEND!" name="submission" style="padding: 10px; font-size:16pt;"></p>
                </form>
            </div>

        </section>
	
</body>
